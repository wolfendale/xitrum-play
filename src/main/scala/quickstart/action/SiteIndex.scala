package quickstart.action

import play.TestController
import xitrum.Action
import xitrum.annotation.GET

@GET("")
class SiteIndex extends PlayAction {
  override def execute(): Unit =
    respondWithPlay(controller[TestController].get)
}

@GET("fail")
class Fail extends PlayAction {
  override def execute(): Unit =
    respondWithPlay(controller[TestController].fail)
}
