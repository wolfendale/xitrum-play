package quickstart.action

import akka.stream.scaladsl.{Keep, StreamConverters}
import io.netty.buffer.{ByteBufInputStream, ByteBufOutputStream}
import io.netty.handler.codec.http.HttpResponseStatus
import play.api.http.{HeaderNames, HttpFilters}
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.typedmap.TypedMap
import play.api.mvc.request.{RemoteConnection, RequestTarget}
import play.api.mvc.{BaseController, EssentialAction, Headers, RequestHeader}
import play.api.{Configuration, Environment, Mode}
import xitrum.FutureAction

import scala.collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.reflect.ClassTag
import scala.util.{Failure, Success}

object PlayAction {

  private val environment = Environment.simple(mode = Mode.Prod)

  private val applicationBuilder = GuiceApplicationBuilder(
    environment = environment,
    loadConfiguration = {
      environment =>
        Configuration.load(
          classLoader = environment.classLoader,
          properties = System.getProperties,
          directSettings = Map(
            "config.resource" -> "play/application.conf"
          ),
          allowMissingApplicationConf = false
        )
    }
  )

  private lazy val application =
    applicationBuilder
      .globalApp(false)
      .build()

  private lazy val requestFactory =
    application.requestFactory

  private lazy val httpFilters: HttpFilters =
    application.injector.instanceOf[HttpFilters]
}

abstract class PlayAction extends FutureAction {

  import PlayAction._

  def controller[A <: BaseController : ClassTag]: A =
    application.injector.instanceOf[A]

  def respondWithPlay(action: EssentialAction): Unit = {

    // apply filters from the play application
    val filteredAction = httpFilters.filters.foldRight(action) {
      (n, m) => n(m)
    }

    val accumulator = filteredAction(playRequestHeader)
    val requestBodySource =
      StreamConverters.fromInputStream(() => new ByteBufInputStream(request.content))

    val futureResult = accumulator.run(requestBodySource)(application.materializer).flatMap {
      result =>

        val header = result.header
        val body = result.body

        response.setStatus(HttpResponseStatus.valueOf(header.status))

        header.headers.foreach {
          case (k, v) =>
            response.headers.add(k, v)
        }

        body.contentType.foreach {
          contentType =>
            response.headers.add(HeaderNames.CONTENT_TYPE, contentType)
        }

        val responseBodySink =
          StreamConverters.fromOutputStream(() => new ByteBufOutputStream(response.content))

        body.dataStream.toMat(responseBodySink)(Keep.right).run()(application.materializer).map {
          ioResult =>
            ioResult.status match {
              case Success(_) =>
                respond()
              case Failure(e) =>
                // passes the error up to Xitrum to handle
                throw e
            }
        }
    }

    // configurable timeout?
    Await.result(futureResult, 5.seconds)
  }

  private lazy val playRequestHeader: RequestHeader = {

    val remoteConnection = RemoteConnection(
      remoteAddressString = remoteIp,
      secure = false,
      clientCertificateChain = None
    )

    val requestTarget = RequestTarget(
      uriString = request.uri,
      path = handlerEnv.pathInfo.decoded,
      queryString = queryParams.toMap
    )

    val playHeaders = Headers(request.headers.iteratorAsString.asScala.toList.map { entry =>
      entry.getKey -> entry.getValue
    }: _*)

    requestFactory.createRequestHeader(
      connection = remoteConnection,
      method = request.method.name,
      target = requestTarget,
      version = request.protocolVersion.text,
      headers = playHeaders,
      attrs = TypedMap.empty
    )
  }
}
