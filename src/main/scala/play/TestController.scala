package play

import com.google.inject.{Inject, Singleton}
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}

@Singleton
class TestController @Inject() (val controllerComponents: ControllerComponents) extends BaseController {

  def get: Action[AnyContent] = Action { implicit request =>
    Ok(Json.obj("foo" -> "bar"))
  }

  def fail: Action[AnyContent] = Action { implicit request =>
    throw new Exception("oh noes")
  }
}
